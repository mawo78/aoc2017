/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codewars;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author mawo7
 */
public class UniqueLetterSubStr {
    public static void main(String[] args) {
        String x = "QWERTYUIOPASDFGHJKLKZXCVBNMQWERTYUIOPASDFGHJKLKZXCVBNMQWERTYUIOPASDFGHJKLKZXCVBNMQWERTYUIOPASDFGHJKLKZXCVBNMQWERTYUIOPASDFGHJKLKZXCVBNM";
        test(x);
        x = "ABC";
        test(x);
        StringBuilder sb = new StringBuilder();
        Random rnd = new Random();        
        for(int i = 0; i < 10000; i++){
            sb.append(Integer.toHexString(rnd.nextInt() % 16).toUpperCase());
        }
        x = sb.toString();
        test(x);
    }
    
    private static void test(String x){
        long res0, res1;
        UniqueLetterSubStr uniq = new UniqueLetterSubStr();
        res0 = uniq.uniqNr(x);
        res1 = uniq.naiveCheck(x);
        System.out.println(res0);
        System.out.println(res1);
    }
            

    long N = 1000000000;//modX;
    private long uniqNr(String in) {
        long res = 0;
        for(char c = 'A'; c <= 'Z'; c++){
            res += uniqueForLetter(c, in) % N;
        }
        return res;
    }
    private long uniqueForLetter(char c, String in){
        long res = 0;
        ArrayList<Integer> pos = new ArrayList<>();
        pos.add(-1);
        for(int i = 0; i < in.length(); i++){
            if(in.charAt(i) == c){
                pos.add(i);
            }
        }
        pos.add(in.length());
        for(int i = 1; i < pos.size() - 1; i++){
            int dL = pos.get(i) - pos.get(i - 1);
            int dR = pos.get(i + 1) - pos.get(i);
            res += dL * dR;
        }
        return res;
    }
    
    private long naiveCheck(String in){
        long res = 0;
        for(char c = 'A'; c <= 'Z'; c++){
            res += naiveUniqueForLetter(c, in) % N;
        }
        return res;
    }

    private long naiveUniqueForLetter(char c, String in){
        long res = 0;
        ArrayList<Integer> ile = new ArrayList<>();
        int pom = 0;
        for(int i = 0; i < in.length(); i++){
            if(in.charAt(i) == c){
                pom++;
            }
            ile.add(pom);
        }
        for(int i = 0; i < in.length(); i++){
            int p;
            if(0 == i)
                p = 0;
            else 
                p = ile.get(i-1);
            for(int j = i ; j < in.length(); j++){
                if(ile.get(j) - p == 1){
                    res++;
                    res %= N;
                }
            }
        }
        return res;
    }
}
