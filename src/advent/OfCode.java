/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package advent;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author wojcimar
 */
public class OfCode {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        OfCode a = new OfCode();
        //a.run();
        //a.pass0();
        //a.day23();
        a.day25();
    }

    class CTurMach{
        char state = 'A';
        Map<Integer, Integer> mem = new HashMap<>();
        int ptr = 0;
        int curVal;

        public CTurMach() {
            mem.put(0, 0);
        }
        
        int count(){
            int c = 0;
            for(Entry<Integer, Integer> ent : mem.entrySet()){
                if(ent.getValue() == 1)
                    c++;
            }
            return c;
        }

        private void step() {
            Integer pom = mem.get(ptr);
            if(null == pom){
                pom = 0;
                mem.put(ptr, 0);
            }
            curVal = pom;
            switch(state) {
                case 'A' : 
                    stateA();
                    break;
                case 'B' : 
                    stateB();
                    break;
                case 'C' : 
                    stateC();
                    break;
                case 'D' : 
                    stateD();
                    break;
                case 'E' : 
                    stateE();
                    break;
                case 'F' : 
                    stateF();
                    break;
                default: 
                    break;
            }
        }

        private void stateA() {
            if(0 == curVal) {
                mem.put(ptr, 1);
                ptr++;
                state = 'B';
            } else if(1 == curVal) {
                mem.put(ptr, 0);
                ptr--;
                state = 'B';
            }
        }
               
        private void stateB() {
            if(0 == curVal) {
                mem.put(ptr, 1);
                ptr--;
                state = 'C';
            } else if(1 == curVal) {
                mem.put(ptr, 0);
                ptr++;
                state = 'E';
            }
        }
               
        private void stateC() {
            if(0 == curVal) {
                mem.put(ptr, 1);
                ptr++;
                state = 'E';
            } else if(1 == curVal) {
                mem.put(ptr, 0);
                ptr--;
                state = 'D';
            }
        }
               
        private void stateD() {
            if(0 == curVal) {
                mem.put(ptr, 1);
                ptr--;
                state = 'A';
            } else if(1 == curVal) {
                mem.put(ptr, 1);
                ptr--;
                state = 'A';
            }
        }
               
        private void stateE() {
            if(0 == curVal) {
                mem.put(ptr, 0);
                ptr++;
                state = 'A';
            } else if(1 == curVal) {
                mem.put(ptr, 0);
                ptr++;
                state = 'F';
            }
        }
               
        private void stateF() {
            if(0 == curVal) {
                mem.put(ptr, 1);
                ptr++;
                state = 'E';
            } else if(1 == curVal) {
                mem.put(ptr, 1);
                ptr++;
                state = 'A';
            }
        }
               
    }
    void day25(){
        int N =12683008 ;
        CTurMach tm = new CTurMach();
        for(int i = 0; i < N; i++){
            tm.step();
        }
        System.out.println(tm.count());
    }
    int maxDl = 0, maxVal = 0;
    private void reqBr(CBrdg b, ArrayList<CBrdg> mosty, int lev, int val) {
        b.bUsed= true;
        b.mVal = b.L + b.R;
        boolean bFin = true;
        for(CBrdg br : mosty){
            if(!br.bUsed && (br.L == b.R || br.R == b.R)) {
                bFin = false;
                if(br.L != b.R){
                    int pom = br.L;
                    br.L = br.R;
                    br.R = pom;
                }
                //if(br.mVal == -1)
                br.mVal = -1;
                    reqBr(br, mosty, lev + 1, val + b.L + b.R);
                b.mVal = Math.max(b.mVal, b.L + b.R + br.mVal);
            }
            
        }
        if(bFin){
            if(maxDl <= lev){
                maxDl = lev;
                maxVal = Math.max(maxVal,  b.L + b.R + val);
                System.out.println(maxDl + " " + maxVal);
            }
        }
        b.bUsed= false;
    }

    class CBrdg {
        boolean bUsed = false;
        int L, R, mVal = -1;
        CBrdg(int x, int y){
            L = x;
            R = y;
        }
        CBrdg(String x, String y){
            L = Integer.parseInt(x);// (x);
            R = Integer.parseInt(y);
            mVal = L + R;
        }
        
    }
    void day24() throws FileNotFoundException, IOException{
        String file = "input24.txt";
        ArrayList<CBrdg> mosty = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] brg = line.split("/");
                mosty.add(new CBrdg(brg[0], brg[1]));
            }
        }
        for(CBrdg b : mosty){
            if(b.L == 0 || b.R == 0) {
                if(b.R == 0){
                    int pom = b.L;
                    b.L = b.R;
                    b.R = pom;
                }
                reqBr(b, mosty, 1, 0);
                System.out.println(b.mVal);
            }//1647 bad //1677 too high//1673 ok
        }
    }
    
    void day22() throws FileNotFoundException, IOException{
        //.##/#../#.. => .#../...#/.##./..#.
        String file = "input22.txt";
        Map<Wsp, Character> infctd = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            int y = 0;
            while ((line = br.readLine()) != null) {
                for(int x = 0; x < line.length(); x++){
                    if(line.charAt(x) == '#'){
                        infctd.put(new Wsp(x, y), '#');
                    }
                }
                y++;
            }
        }
        //printVir(infctd);
        
        int x = 25/2;
        int y = 25/2;
        Wsp vir = new Wsp(x, y);
        int dx = 0, dy = -1;
        int N = 10000000;
        int ile = 0;
        for(int i = 0; i < N; i++){
            Character c = infctd.get(vir);
            if(null == c || c == '.'){
                int pom = dx;
                dx = dy;
                dy = -pom;
                infctd.put(vir, 'W');
            } else if(c == 'W'){
                infctd.put(vir, '#');
                ile++;
            } else if(c == 'F'){
                dx = -dx;
                dy = -dy;
                infctd.remove(vir);// put(vir, '.');
            }else if(c == '#'){
                int pom = dx;
                dx = -dy;
                dy = pom;
                infctd.put(vir, 'F');
            }  
            vir = new Wsp(vir.x + dx, vir.y + dy);
            //printVir(infctd);
            if(0 == i % 1000000)
                System.out.println(i);
        }
        //printVir(infctd);
        System.out.println(ile);
    }

    private void printVir(Map<Wsp, Character> infctd) {
        int minx = Integer.MAX_VALUE;
        int miny = Integer.MAX_VALUE;
        int maxx = Integer.MIN_VALUE;
        int maxy = Integer.MIN_VALUE;
        for(Wsp w : infctd.keySet()){
            minx = Math.min(minx, w.x);
            miny = Math.min(miny, w.y);
            maxx = Math.max(maxx, w.x);
            maxy = Math.max(maxy, w.y);
        }
        for (int y = miny; y <= maxy; y++) {
            for (int x = minx; x <= maxx; x++) {
                final Wsp wsp = new Wsp(x,y);
                if(infctd.containsKey(wsp))
                    System.out.print(infctd.get(wsp));
                else
                    System.out.print(".");
            }        
            System.out.println();
        }
        System.out.println();
    }
    
    class CSquare{
        ArrayList<String> lns = new ArrayList<>();
        int rowSize = 0;
        public void init(String s){
            String[] sq = s.split("/");
            for(String s1 : sq){
                lns.add(s1);
            }
            rowSize = lns.size();
        }

        @Override
        public int hashCode() {
            int hash = 7;
            for(int i = 0; i < lns.size(); i++){
                hash ^= lns.get(i).hashCode();
            }
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final CSquare other = (CSquare) obj;
            if (!Objects.equals(this.lns, other.lns)) {
                return false;
            }
            return true;
        }

        private CSquare rotate() {
            //ArrayList<String> pom = new ArrayList<>(lns);
            CSquare res = new CSquare();
            for(int i = 0; i < rowSize; i++) {
                StringBuilder sb = new StringBuilder();
                for(int j = 0; j < rowSize; j++) {
                    sb.append(lns.get(j).charAt(rowSize - i - 1));
                }
                res.lns.add(sb.toString());
            }
            res.rowSize = rowSize;
            return res;
        }

        private CSquare mirror() {
            CSquare res = new CSquare();
            for(int i = 0; i < rowSize; i++) {
                res.lns.add(lns.get(rowSize - i - 1));
            }
            res.rowSize = rowSize;
            return res;
        }

        @Override
        public String toString() {
            StringBuffer sb = new StringBuffer();
            for(int i = 0; i < lns.size(); i++) {
                sb.append(lns.get(i));
                sb.append("\n");
            }
            sb.append("\n");

            return sb.toString();
        }

        private CSquare subSquare(int x, int y, int step) {
            CSquare res = new CSquare();
            res.rowSize = step;
            for(int i = 0; i < step; i++) {
                res.lns.add(lns.get(i + y).substring(x,  x + step));
            }
            return res;
        }

        private void add(int x, int y, CSquare res) {
            int step = res.rowSize;
            if(0 == x) {
                for(int i = 0; i < step; i++) {
                    lns.add(res.lns.get(i));
                }
            } else {
                for(int i = 0; i < step; i++) {
                    String pom = lns.get(y + i);
                    lns.set(y + i, pom + res.lns.get(i));
                }
            }
        }

        int count(){
            int res = 0;
            for(int i = 0; i < rowSize; i++) {
                String pom = lns.get(i);
                for(int j = 0; j < rowSize; j++) {
                    if(pom.charAt(j) == '#')
                        res++;
                }
            }
            return res;
        }
        
    }
    void day21() throws FileNotFoundException, IOException{
        //.##/#../#.. => .#../...#/.##./..#.
        String file = "input21.txt";
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            Map<CSquare, CSquare> rules = new HashMap<>();
            //Map<CSquare, CSquare> rules3 = new HashMap<>();
            while ((line = br.readLine()) != null) {
                String[] prod = line.split(" => ");
                String l = prod[0];
                String r = prod[1];
                
                CSquare sl = new CSquare();
                sl.init(l);

                CSquare sr = new CSquare();
                sr.init(r);
                
                for(int i = 0; i < 4; i++){
                    sl = sl.rotate();
                    System.out.println(sl);
                    rules.put(sl, sr);
                }
                sl = sl.mirror();
                for(int i = 0; i < 4; i++){
                    sl = sl.rotate();
                    System.out.println(sl);
                    rules.put(sl, sr);
                }
            }
            
            CSquare cnv = new CSquare();
            cnv.init(".#./..#/###");
            boolean b1 = cnv.equals(cnv);
            for(int it = 0; it < 18; it++){
                //System.out.println(cnv);
                int dx = 0;
                if(0 == cnv.rowSize % 2){
                    dx = 2;
                } else {
                    dx = 3;
                }
                int step = cnv.rowSize / dx;
                CSquare pom = new CSquare();
                pom.rowSize = (cnv.rowSize / dx) * (dx + 1);
                for(int i = 0; i < cnv.rowSize / dx; i++){
                    for(int j = 0; j < cnv.rowSize / dx; j++){
                        CSquare ssq = cnv.subSquare(i * dx, j * dx, dx);
                        CSquare res = rules.get(ssq);
                        if(null != res){
                            //System.out.println("^^^^^^");
                            //System.out.println(pom);
                            pom.add(i* res.rowSize, j* res.rowSize, res);
                            //System.out.println(pom);
                            //System.out.println("_______");
                        }
                    }                                                
                }
                cnv = pom;

            }
            System.out.println(cnv);
            System.out.println(cnv.count());
        }
    }
    
    class CPoint3d {
        ArrayList<Long> pos = new ArrayList<>(3);
        ArrayList<Long> vel = new ArrayList<>(3);
        ArrayList<Long> acc = new ArrayList<>(3);
        int indx;
        
        //p=<5528,2008,1661>, v=<-99,-78,-62>, a=<-17,-2,-2>
        void init(String s, int i){
            String[] vals = s.split(" ");
            init3p(vals[0].trim(), pos);
            init3p(vals[1].trim(), vel);
            init3p(vals[2].trim(), acc);
            indx = i;
        }

        private void init3p(String s, ArrayList<Long> pos) {
            s = s.substring(3, s.indexOf(">"));
            String[] xyz = s.split(",");
            for(int i = 0; i < xyz.length; i++){
                pos.add(Long.parseLong(xyz[i]));
            }
        }
        long manhat(){
            return Math.abs(pos.get(0)) + Math.abs(pos.get(1)) + Math.abs(pos.get(2));
        }
        void step(){
            for(int i = 0; i < 3; i++){
                long v = vel.get(i);
                long a = acc.get(i);
                long x = pos.get(i);
                v += a;
                x += v;
                pos.set(i, x);
                vel.set(i, v);
            }
        }

        @Override
        public int hashCode() {
            int hash = 7;
            if(null != pos){
                for(int i = 0;i < pos.size(); i++){
                    hash ^= pos.get(i);
                }
            }
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final CPoint3d other = (CPoint3d) obj;
            if(this.pos.size() != other.pos.size()){
                return false;
            }
            for(int i = 0;i < pos.size(); i++){
                if(!Objects.equals(this.pos.get(i), other.pos.get(i)))
                    return false;
            }

            return true;
        }
        
    }
    void day20() throws FileNotFoundException, IOException{
        Set<CPoint3d> points = new HashSet<>();
        String file = "input20.txt";
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            int i = 0;
            while ((line = br.readLine()) != null) {
                CPoint3d pnt = new CPoint3d();
                pnt.init(line, i++);
                points.add(pnt);
            }
        }
        
        for(int step = 0 ; step < 10000; step++){
            long minI = -1, minV = Integer.MAX_VALUE;
            Set<CPoint3d> newPoints = new HashSet<>();
            Set<CPoint3d> pntsToRemove = new HashSet<>();
            for(CPoint3d pnt : points){                
                pnt.step();
                if(newPoints.contains(pnt)) {
                    pntsToRemove.add(pnt);
                } else {
                    newPoints.add(pnt);
                }
//                if(pnt.manhat() < minV){
//                    minI = i;
//                    minV = pnt.manhat();
//                }
            }
            for(CPoint3d pnt : pntsToRemove){
                newPoints.remove(pnt);
            }
            points = newPoints;
            System.out.println(step + " " + points.size());
            //513 too high
            //468 too high
        }
        
    }
    void day19() throws FileNotFoundException, IOException{
        ArrayList<String> map = new ArrayList<>();
        String file = "input19.txt";
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                map.add(line);
            }
        }
        int Y = 0, X = 0, dX = 0, dY = 1, pom; //down
        //start
        int ile = 0;
        String fstLine = map.get(0);
        X = fstLine.indexOf("|");
        while(X >= 0 && Y >= 0 && X < fstLine.length() && Y < fstLine.length()){            
            while(getZn(map, X, Y) != '+'){
                char c = getZn(map, X, Y);
                if(c != '-' &&c != '|' &&c != '+'){
                    System.out.println("[" + c);
                    if(c == 'Y') {
                        System.out.println(ile);
                        System.exit(0);
                    }
                }
                X += dX;
                Y += dY;
                ile++;
            }
            pom = dX;
            dX = dY;
            dY = -pom;
            if(getZn(map, X + dX, Y + dY) != ' '){
                X += dX;
                Y += dY;
                ile++;
                continue;
            }
            dX = -dX;
            dY = -dY;
            
            X += dX;
            Y += dY;
            ile++;
        }
    }
    
    private char getZn(ArrayList<String> map, int X, int Y) {
        String row = map.get(Y);
        //System.out.println(row.charAt(X));
        return row.charAt(X);
    }
    int mul;
    int interpret23(int PC, ArrayList<String> code, Map<String, Long> reg){
        String line = code.get(PC);
        String[] words = line.split(" ");
        if (words[0].equals("set")){            
            long v = getValue(reg, words[2]);
            reg.put(words[1], v);
        } else if (words[0].equals("sub")){            
            long v = regVal(reg, words[1]);
            v -= getValue(reg, words[2]);
            reg.put(words[1], v);
        } else if (words[0].equals("mul")){            
            long v = regVal(reg, words[1]);
            v *= getValue(reg, words[2]);
            reg.put(words[1], v);
        } else if (words[0].equals("jnz")){            
            long v = getValue(reg, words[1]);
            if(v != 0) {
                return (int)(PC + getValue(reg, words[2]));
            }
        }
        
        return PC + 1;
    }

    void day23() throws FileNotFoundException, IOException{
        
        String file = "input23.txt";
        Map<String, Long> reg = new HashMap<>();
        ArrayList<String> code = new ArrayList<>();
        
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                code.add(line);
            }
        }
        int PC = 0, step = 0;
        mul = 0;
        reg.put("a", 1l);
        while(step < 50 && PC >= 0 && PC < code.size()) {
            PC = interpret23(PC, code, reg);
            
            step++;
           // if(0 == (step % 100000)) {
                System.out.println("\t" + PC + " " + step + " " + reg.get("h") + " " + reg);
            //}
        }    
        System.out.println(reg.get("h"));
    }    
    
    long day23_p2(){
        long a = 1l,b=0,c=0,d=0,e=0,f=0,g=0,h=0;
//        set b 65
        b = 65l;
//set c b
        c = b;
//jnz a 2
//jnz 1 5
//mul b 100
        b *= 100L;
//sub b -100000
        b += 100000;
//set c b
        c = b;
//sub c -17000
        c += 17000;
//set f 1
do{        f = 1;
//set d 2
        d = 2;
//set e 2
do{    e = 2;
//set g d
    do{//g = d;
//mul g e
//        g *= e;
//sub g b
//        g -= b;
//jnz g 2
//set f 0
        if(d*e-b == 0){
            f = 0;
            //break;
        }
//sub e -1
        e++;
//set g e
        //g = e;
//sub g b
        //g -= b;
//jnz g -8
    }while(e <= Math.sqrt(b));//g != 0);
//sub d -1
    d++;
//set g d
    //g = d;
//sub g b
    //g = d - b;
//jnz g -13
    }while(d != b);// (g!=0);
//jnz f 2
//sub h -1
    if(f == 0){
        h++;
        System.out.println(h);
    }
//set g b
    g = b;
//sub g c
    g -=c;
//jnz g 2
//jnz 1 3
    if(g == 0)
        return h;
//sub b -17
    b += 17;
    System.out.println(b);
//jnz 1 -23
}while(true);
    }
    
    long pr1Sent = 0;
    void day18() throws FileNotFoundException, IOException{
        String file = "input18.txt";
        Map<String, Long> reg = new HashMap<>();
        Map<String, Long> reg1 = new HashMap<>();
        ArrayList<String> code = new ArrayList<>();
        
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                code.add(line);
            }
        }
        
        int PC = 0, PC1 = 0;
        List<Long> q0 = new LinkedList<>();
        List<Long> q1 = new LinkedList<>();
        int step = 0;
        reg.put("p", 0L);
        reg1.put("p", 1L);
        while(PC >= 0 && PC < code.size() && PC1 >= 0 && PC1 < code.size()) {
            PC = interpret(PC, code, reg, q1, q0, 0);
            PC1 = interpret(PC1, code, reg1, q0, q1, 1);
            
            step++;
            if(0 == (step % 1000000)) {
                System.out.println("\t" + PC + " " + PC1 + " " + step + " " + q0.size() + " " + q1.size() + " " + pr1Sent);
            }
        }
        // 7481 too low
    }
    long regVal(Map<String, Long> reg, String r) {
        Long v = reg.get(r);
        if(null == v){
            reg.put(r, 0L);
            return 0;
        } else
            return v;
    }
    long getValue(Map<String, Long> reg, String r){
        long v = 0;
        boolean bParsed = false;
        try {
            v = Integer.parseInt(r);
            bParsed = true;
        } catch (NumberFormatException ignore){
        }
        if(!bParsed){
            v = regVal(reg, r);
        }
        return v;
    }
    int interpret(int PC, ArrayList<String> code, Map<String, Long> reg, List<Long> queueIn, List<Long> queueOut, int ID){
        String line = code.get(PC);
        String[] words = line.split(" ");
        if(words[0].equals("snd")){
            //reg.put("played", regVal(reg, words[1]));
            queueOut.add(regVal(reg, words[1]));
            if(1 == ID){
                pr1Sent++;
            }
            //System.out.print(">");
        } else if (words[0].equals("set")){            
            long v = getValue(reg, words[2]);
            reg.put(words[1], v);
        } else if (words[0].equals("add")){            
            long v = regVal(reg, words[1]);
            v += getValue(reg, words[2]);
            reg.put(words[1], v);
        } else if (words[0].equals("mul")){            
            long v = regVal(reg, words[1]);
            v *= getValue(reg, words[2]);
            reg.put(words[1], v);
        } else if (words[0].equals("mod")){            
            long v = regVal(reg, words[1]);
            v %= getValue(reg, words[2]);
            reg.put(words[1], v);
        } else if (words[0].equals("rcv")){     
            if(!queueIn.isEmpty()){
                Long v = queueIn.get(0);
                queueIn.remove((int)0);
                reg.put(words[1], v);
                //System.out.print("<");
            } else {
                PC--;
                //System.out.print("|");
                //System.out.println(pr1Sent);
            }
            /*long v = regVal(reg, words[1]);
            if(v > 0) {
                System.out.println(regVal(reg, "played"));
                return -1;
            }*/
        } else if (words[0].equals("jgz")){            
            long v = getValue(reg, words[1]);
            if(v > 0) {
                return (int)(PC + getValue(reg, words[2]));
            }
        }
        
        return PC + 1;
    }
    
    void day15(){
        long A = 883, B = 879;
        //long A = 65, B = 8921;
        long Af = 16807, Bf = 48271, M = 2147483647;
        int ile = 0;
        for(int i = 0; i < 5000000; i++){
            do {
                A = (A * Af) % M;
            }while((A & 0x3) != 0);
            do {
                B = (B * Bf) % M;
            }while((B & 0x7) != 0);
            long x = A & 0xffff;
            long y = B & 0xffff;
            if(x == y)
                ile++;
        }
        System.out.println(ile);
    }
    
    void day13() throws FileNotFoundException, IOException{
        String file = "input13.txt";
        int ile = 0;
        int N = 93;
        int firewalls[] = new int[N];
        int pos[] = new int[N];
        for(int i = 0; i < N; i++){
            firewalls[i] = 0;
            pos[i] = 0;
        }
        
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] words = line.split(": ");
                int x = Integer.parseInt(words[0]);
                int y = Integer.parseInt(words[1]);
                firewalls[x] = y;
            }
        }
        boolean goUp[] = new boolean[N];
        for(int i = 0; i < N; i++)
            goUp[i] = true;
        
        for(int i = 0; i < N; i++) {
            if(firewalls[i] > 0 && pos[i] == 0) {
                ile += i * firewalls[i];
            }
            
            for(int j = 0; j < N ; j++){
                if(firewalls[j] > 1) {
                    if(goUp[j]){
                        pos[j]++;
                        if(pos[j] == firewalls[j] - 1){
                            goUp[j] = false;
                        }
                    } else {
                        pos[j]--;
                        if(pos[j] == 0){
                            goUp[j] = true;
                        }
                    }
                }
            }
        }
        
        System.out.println("" + ile);        //200 TOO LOW
    }
    
    void cod(){
        StringBuilder sb = new StringBuilder();
        Random rand = new Random();
        int N = 100000;
        for(int i = 0; i < N; i++){
            sb.append((char)('A' + (rand.nextInt(26))));
        }
        //String s = "ABDAC";//sb.toString();
        String s = sb.toString();
        int sum = 0;
        HashMap<Character, Integer> hm = new HashMap<>();
        for(int lvl = 1; lvl <= N; lvl++) {
            //for(int i = 0; i < 26; i++){
            //    ht[i] = 0;
            //}
            hm.clear();
            Integer unique = 0;
            for(int i = 0; i < lvl; i++) {
                Integer x = hm.get(s.charAt(i));
                if(null == x)
                    x = 0;
                hm.put(s.charAt(i), x + 1);
                if(x == 0)
                    unique++;
                else
                    unique--;
            }
            sum += unique % 1000000007;
            sum %= 1000000007;
            for(int i = 1 ;i < N - lvl; i++) {
                unique = sub(s.charAt(i - 1), unique, hm);
                unique = add(s.charAt(i + lvl - 1), unique, hm);
                sum += unique % 1000000007;
                sum %= 1000000007;
            }
            System.out.println(lvl+ " " + sum);
        }
        System.out.println(sum);
    }

    private int sub(char a, int unique, HashMap<Character, Integer> hm) {
        Integer x = hm.get(a);
        if(null == x)
            x = 0;
        hm.put(a, x - 1);
        if(x == 2)
            unique++;
        else
            unique--;
        return unique;
    }

    private int add(char a, int unique, HashMap<Character, Integer> hm) {
        Integer x = hm.get(a);
        if(null == x)
            x = 0;
        hm.put(a, x + 1);
        if(x == 0)
            unique++;
        else
            unique--;
        return unique;
    }

    class Wsp {
        int x, y;
        Wsp(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public int hashCode() {
            return x ^ y;
        }

        @Override
        public boolean equals(Object obj) {
            if(null == obj)
                return false;
            Wsp pom = (Wsp)obj;
            return pom.x == x && pom.y == y;
        }
        
    }
    private void run() {
        Map< Wsp, Integer> tab = new HashMap<>(); 
        Wsp w = new  Wsp(0,0);
        tab.put(new Wsp(0,0), 1);
        for(int i = 2; i <= 100; i+=2) {
            //początek w prawo
            w.x += 1;
            //i do góry i razy
            for(int j = 0; j < i; j++) {
                gather(w, tab);
                w.y += 1;
            }
            w.y -= 1;
            //teraz w lewo:
            w.x -= 1;
            for(int j = 0; j < i; j++) {
                gather(w, tab);
                w.x -= 1;
            }
            w.x += 1;
            //w dól:
            w.y -= 1;
            for(int j = 0; j < i; j++) {
                gather(w, tab);
                w.y -= 1;
            }
            w.y += 1;
            // w lewo:
            w.x += 1;
            for(int j = 0; j < i; j++) {
                gather(w, tab);
                w.x += 1;
            }
            w.x -= 1;
        }
    }
    
    private int getVal(int x, int y, Map<Wsp, Integer> tab) {
        Wsp w = new Wsp(x,y);
        //w.x -= 1;
        Integer p = tab.get(w);
        if(null != p) {
            return p;
        }
        else {
            return 0;
        }
    }
    
    private void gather(Wsp w, Map<Wsp, Integer> tab) {
        int sum = 0;
        sum += getVal(w.x + 1, w.y, tab);
        sum += getVal(w.x, w.y + 1, tab);
        sum += getVal(w.x - 1, w.y, tab);
        sum += getVal(w.x, w.y - 1, tab);
        sum += getVal(w.x + 1, w.y + 1, tab);
        sum += getVal(w.x + 1, w.y - 1, tab);
        sum += getVal(w.x - 1, w.y + 1, tab);
        sum += getVal(w.x - 1, w.y - 1, tab);
        tab.put(new Wsp(w.x, w.y), sum);
        if(sum > 312051)
            System.out.println("" + w.x + " " + w.y + " " +sum);
    }

    void pass0() throws FileNotFoundException, IOException{
        String file = "input.txt";
        int ile = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] words = line.split(" ");
                Set<String> pom = new HashSet<>();
                for(String s : words) {
                    char[] chars = s.toCharArray();
                    Arrays.sort(chars);
                    String sorted = new String(chars);
                    pom.add(sorted);
                }
                if(pom.size() == words.length)
                    ile++;
            }
        }        
        System.out.println("" + ile);
    }
    
    void jump0() throws FileNotFoundException, IOException{
        String file = "input5.txt";
        ArrayList<Integer> tab = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                int x = Integer.parseInt(line);
                tab.add(x);
            }
        }
        
        int step = 0;
        int pos = 0;
        while(pos < tab.size() && pos >= 0) {
            int jmp = tab.get(pos);
            if(jmp >= 3) {
                tab.set(pos, jmp - 1);
            } else {
                tab.set(pos, jmp + 1);
            }
            pos += jmp;
            step++;
        }
        System.out.println(step);
    }
    
    class Mem {
        ArrayList<Integer> tab;
        boolean calc = false;
        int hash = 0;
        int step = 0;
        Mem(ArrayList<Integer> x, int s){
            tab = new ArrayList<>(x);
            step = s;
        }
        @Override
        public int hashCode() {
            if(!calc) {
                for(int t : tab){
                    hash ^= t;
                }
            }
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if(null == obj)
                return false;
            Mem pom = (Mem)obj;
            if(pom.tab.size() != tab.size())
                return false;
            for(int i = 0; i < tab.size(); i++){
                if(tab.get(i) != pom.tab.get(i))
                    return false;
            }
            return true;
        }

    }
    
    void memory(){
        String pom = 
"4	10	4	1	8	4	9	14	5	1	14	15	0	15	3	5";
//"0	2	7	0";
        String[] t = pom.split("\t");
        ArrayList<Integer> mem = new ArrayList<>();
        for(String x : t) {
            int y = Integer.parseInt(x);
            mem.add(y);
        }
        
        int step = 0;
        Set<Mem> m = new HashSet<Mem>();
        m.add(new Mem(mem, step));
        
        System.out.println(Arrays.toString(mem.toArray()));
        do {
            int pos = findMaxFirst(mem);
            distribute(pos, mem);
            step++;
            System.out.println(Arrays.toString(mem.toArray()));
            if(m.contains(new Mem(mem, 0))) {
                break;
            }
            m.add(new Mem(mem, step));
        } while(true);
        
        for (Iterator<Mem> it = m.iterator(); it.hasNext(); ) {
            Mem f = it.next();
            if (f.equals(new Mem(mem, 0)))
                System.out.println(step - f.step);
        }
        
    }
    private int findMaxFirst(ArrayList<Integer> mem) {
        int pos = -1, maxVal = -1;
        for(int i = 0; i < mem.size(); i++) {
            if(mem.get(i) > maxVal) {
                maxVal = mem.get(i);
                pos = i;
            }
        }
        return pos;
    }

    private void distribute(int pos, ArrayList<Integer> mem) {
        int x = mem.get(pos);
        mem.set(pos, 0);
        while(x > 0) {
            pos++;
            pos %= mem.size();
            mem.set(pos, mem.get(pos) + 1);
            x--;
        }
    }

}
